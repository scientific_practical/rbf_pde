# Create nodes
# Construct matrix
# solve interpolation (numpy.linalg.solve)
# (compute condition number)
# error analysis (interpolation error on arbitrary different nodes)
# visualize

import numpy as np
from scipy.spatial.distance import cdist
from numpy.random import default_rng


def phi_gaussian(r, scale=1):
    return np.exp(-(r/scale)**2)


def interpolation(nodes, phi, rhs, regpar=None):
    """ interpolation with rbf

    """
    # create distance matrix
    # distm_ij = ||x_i - x_j||
    distm = cdist(nodes, nodes)
    # assembly of matrix
    A = phi(distm)
    # construct y (rhs)
    # A y = rhs
    # A* A y = A* rhs
    # Minimizes ||A y - rhs||^2
    # (A* A + regpar * Identity-Matrix) y = A* rhs
    if regpar is None:
        lamb = np.linalg.solve(A, rhs)
    else:
        lamb = np.linalg.solve(A.T @ A + regpar * np.eye(A.shape[0]), A.T @ rhs)
    return lamb


def apply_interp(lamb, phi, nodes, gridpoints):
    # lets choose 2d domain \omega for interpolation
    distm_interp = cdist(to_node_list(gridpoints), nodes)
    interpolated_vals = phi(distm_interp) @ lamb
    return interpolated_vals.reshape(gridpoints[0].shape)


def to_node_list(gridpoints):
    xx, yy = gridpoints
    return np.stack([xx.ravel(), yy.ravel()], axis=1)


def createnodes(nrnodes):
    """ function Creates nodes
    Parameters:
        nrnodes:
    Return:
        nodes, shape (m,2)
    random numbers for the node generation inside choosen region \omega
    """
    rng = default_rng()
    nodes = rng.random((nrnodes, 2))
    return nodes


def separation_distance(nodes):
    dists = cdist(nodes, nodes)
    np.fill_diagonal(dists, np.inf)
    return np.min(dists)


def condition_number(nodes, rbf):
    dists = cdist(nodes, nodes)
    return np.linalg.cond(rbf(dists))


if __name__ == "__main__":
    nodes = createnodes(50)
    y = np.sin(nodes[:, 0]) * np.cos(nodes[:, 1])
    coeffs = interpolation(nodes, phi_gaussian, y)

# RBF for PDE (example project)

## install

``` bash
poetry install
```

## usage

``` bash
poetry run python
```

eventually you need to set the python-path:
```
sys.path.append(pathtorbf_pde)
```

``` python
import rbf_pde
```

## Markdown <-> notebook

the markdown files in the notebook directory can directly be used in jupyter notebook
and will be updated when the notebook changes.

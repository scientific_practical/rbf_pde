---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.12
    jupytext_version: 1.6.0
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Radial basis functions for Interpolation

To introduce RBF we start with interpolation.

**References:**

* Robert Schaback, *A Practical Guide to Radial Basis Functions* (see literature folder)

## Interpolation

Given node points $x_k \in \Omega \subset \mathbb{R}^d$ and values $y_k \in \mathbb{R}$ for $k = 1, \dots, n$, determine a function $f \colon \Omega \to \mathbb{R}$ such that

$$ f(x_k) = y_k \qquad (k = 1, \dots, n). $$

Often, $f$ is taken as a linear combination of *basis functions* $(F_k)_{k=1}^n$:

$$ f(x) = \sum_{k=1}^n \lambda_k F_k(x). $$

## Radial Basis Functions (RBFs)

To avoid singularities, it makes sense to have the basis $(F_k)$ depend on the nodes $(x_k)$ in a suitable way. One possible choice are radially symmetric functions centered on the nodes:

$$ f(x) = \sum_{k=1}^n \lambda_k \Phi(\lVert x - x_k \rVert), $$

where $\Phi \colon \mathbb{R}_{+} \to \mathbb{R}$ is called a *radial basis function*. Usually $\Phi$ is chosen such that $x \mapsto \Phi(\lVert x - y \rVert)$ is sufficiently smooth on $\mathbb{R}^d$ for all $y$, in particular at the origin $x = y$.

Choices for RBFs include

- **Gaussian**: $\Phi(r) = \exp(-r^2)$
- **Multiquadric**: $\Phi(r) = \sqrt{1 + r^2}$
- **Inverse Quadratic**: $\Phi(r) = (1 + r^2)^{-1}$
- ...

## Scaling

Depending on the density of nodes, it makes sense to use $\Phi(r/c)$ instead of $\Phi(r)$ for some scaling factor $c > 0$.

## Linear System

Determining the coefficients $(\lambda_k)$ for the interpolant from node points $(x_k)$, values $(y_k)$ and (scaled) RBF $\Phi$ in the most straight-forward way amounts to solving the linear equation

$$ A \lambda = y, $$

where $A \in \mathbb{R}^{n \times n}$ is the matrix with coefficients

$$ a_{jk} = \Phi(\lVert x_j - x_k \rVert). $$

This matrix can be shown to be non-singular for many choices of RBF. It is however usually *ill-conditioned*, i.e. determining $\lambda$ from $y$ is numerically unstable.

Since we do not usually care for $\lambda$ itself, but only for the values of the interpolant $f(x)$ (at points other than the nodes $x_k$), this not always a problem in practice. If it is, there are ways to stabilize the solution ($\longrightarrow$ later).


***
**practical**

* Create nodes
* Construct matrix
* solve interpolation (`numpy.linalg.solve`)
* (compute condition number)
* error analysis (interpolation error on arbitrary different nodes)
* visualize

***

```{code-cell} ipython3
import sys
sys.path.append("../")
%load_ext autoreload
%autoreload 1
%aimport rbf_pde
```

```{code-cell} ipython3
import numpy as np
import holoviews as hv
hv.extension("bokeh")
```

```{code-cell} ipython3
f = lambda x: np.sin(x[:, 0])*np.cos(x[:, 1]) # true function
rbfpoints = np.random.rand(20, 2)  # points with known function
bounds = (0,0,1,1)
# points where to interpolate (on a grid for better visualization)
nrp = 30 # number of points per axis
X, Y = np.mgrid[0:1:nrp*1j, 0:1:nrp*1j]
gridpoints = np.vstack([X.ravel(), Y.ravel()]).T  # this is needed to get an array (mx2) of points
res = rbf_pde.interpolation(rbfpoints, gridpoints, f(rbfpoints)) # interpolated values

ytrue = f(gridpoints)  # true solution on gridpoints
```

```{code-cell} ipython3
hv.Image(res.reshape((nrp,nrp)), bounds=bounds )+hv.Image(ytrue.reshape((nrp,nrp)), bounds=bounds)
```

```{code-cell} ipython3
options = { 'Image': dict(colorbar=True) }
hv.Points(rbfpoints) + hv.Image(abs(res-ytrue).reshape((nrp,nrp)), bounds=bounds).options(options)
```

```{code-cell} ipython3

```

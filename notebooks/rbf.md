---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.12
    jupytext_version: 1.6.0
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Radial basis functions for PDE

The example project is about using radial basis functions (RBF) for solving partial differential equations (PDE).


References:

* Robert Schaback, *A Practical Guide to Radial Basis Functions* (included in repository)
* Bengt Fornberg and Natasha Flyer, *Solving PDEs with radial basis functions* [doi](https://doi.org/10.1017/S0962492914000130)
* Cécile Piret, *The orthogonal gradients method: A radial basis functions method for solving partial differential equations on arbitrary surfaces* [doi](https://doi.org/10.1016/j.jcp.2012.03.007)


## Solving PDEs with RBFs

Solving PDEs can be approached in a very similar way to solving the interpolation problem. $f$ is taken a a linear combination of RBFs as above,

$$ f(x) = \sum_{k=1}^n \lambda_k \Phi(\lVert x - x_k \rVert), $$

but instead of requiring $f(x_k) = y_k$, the respective PDE is used to determine $\lambda$.

### Example: Poisson Equation

Let $\Omega \subset \mathbb{R}^2$ be a domain. The poisson equation with Dirichlet boundary condition on $\Omega$ is

$$ \begin{aligned}
\Delta f &= u \qquad \text{in $\Omega$} \\
f &= v \qquad \text{on $\partial \Omega$},
\end{aligned} $$

where $\Delta$ is the Laplace operator. Pick nodes $(x_k)_{k=1}^n$ such that $x_k$ is in the interior of $\Omega$ for $k=1, \dots, m$ and on the boundary of $\Omega$ for $k = m+1, \dots, n$. Then $\lambda$ is determined by the equations

$$ \begin{aligned}
\Delta f(x_k) &= u(x_k) \qquad k = 1, \dots, m \\
f(x_k) &= v(x_k) \qquad k = m+1, \dots, n.
\end{aligned} $$

Note that

$$ \Delta f(x) = \sum_{k=1}^n \lambda_k \Delta \Phi(\lVert x - x_k \rVert), $$

where by slight abuse of notation $\Delta \Phi$ is the radial part of the (radially symmeric) function that results from applying the Laplace operator to $x \mapsto \Phi(\lVert x \rVert)$. More concretely, in $d$ dimensions,

$$ \Delta \Phi(r) = \Phi''(r) + \frac{d - 1}{r} \Phi'(r). $$

The singularity at $r = 0$ is artificial (i.e. $\Phi'(r) \to 0$ as $r \to 0$), but can be a problem in implementations nevertheless. It is often better to rewrite $\Phi$ in terms of a different basis function $\eta$ as

$$ \Phi(r) = \eta\left( \frac{r^2}{2} \right), $$

e.g. for the Gaussian RBF $\eta(t) = \exp(-2t)$, and expand $f$ in terms of $\eta$ in the corresponding way. Then, using the chain rule,

$$ \Delta \eta(t) = 2 t \eta''(r) + d \eta'(t) $$

has no artificial singularity.

### Different choice of basis functions

Another option is to use a different expansion for $f$:

$$ f(x) = \sum_{k=1}^m \lambda_k \Delta \Phi(\lVert x - x_k \rVert) + \sum_{k=m+1}^n \lambda_k \Phi(\lVert x - x_k \rVert), $$

leading to an equation of the form (in suggestive notation)

$$ \begin{pmatrix}
\Delta^2 \Phi & \Delta \Phi \\
\Delta \Phi & \Phi
\end{pmatrix}
\lambda =
\begin{pmatrix}
u \\ v
\end{pmatrix}, $$

where the upper row uses to the first $m$ nodes in the interior and the lower row the remaining $n-m$ nodes on the boundary. The resulting matrix is symmetric, which can be useful for some numerical solvers and for theoretical investigations.

***
**practical**

* create nodes (issues with node generation?)
* construct linear system for poisson equation
* solve system
* error analysis
* visualize

***

```{code-cell} ipython3
%load_ext autoreload
%autoreload 1
%aimport rbf_pde
```

```{code-cell} ipython3
import numpy as np
import holoviews as hv
hv.extension("bokeh")
```

```{code-cell} ipython3
nodes = rbf_pde.Nodes(10)

pX, pY = np.mgrid[0:1:100j, 0:1:100j]
#print(pX.flatten())
points = np.vstack((pX.flatten(),pY.flatten())).T
res = rbf_pde.poisson(nodes, points)
print(res.shape)
```

```{code-cell} ipython3
hv.Raster(res, nodes)
```

```{code-cell} ipython3
nodes.boundary
```

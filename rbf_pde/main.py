from rbf_pde import Nodes
from rbf_pde import poisson

if __name__ == "__main__":
    nodes = Nodes(100)
    poisson(nodes)

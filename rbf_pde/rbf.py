#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: jschulz1
"""

import numpy as np


class Gaussian:
    def __init__(self, scale=1):
        self.scale = scale
        return None

    def __call__(self, r, derivative=0):
        # return scale*np.exp(r**2)*(2*r)**(derivative)
        return self.scale*np.exp(-2*r)*(-2)**(derivative)

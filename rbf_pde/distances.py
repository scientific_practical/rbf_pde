#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: cruegge
"""

import numpy as np


def distsquared(x, y=None):
    """Compute the squared distance matrix

    Computes the matrix A = (a_ij), where

        a_ij = 1/2 * || x_i - y_j ||^2

    Parameters
    ----------
    x, y:
      Arrays of shape (n, d) and (m, d), respectively,
      interpreted as collections of n (resp. m) points
      in R^d.

      y is optional. distsquared(x) is equivalent to
      distsquared(x, x).

    Returns
    -------
    Array of shape (n, m)
    """

    if y is None:
        y = x

    # Compute matrix of all differences x_i - y_j, shape (n, m, d).
    # First, x is reshaped to (n, 1, d) by inserting a dimension, then
    # the difference matrix is computed via broadcasting.
    differences = x[:, None, :] - y

    # Compute matrix of norms || x_i - y_j ||
    norms = np.linalg.norm(differences, axis=2)

    return 1/2 * norms**2


def distsquaredlib(x, y=None):
    """ Compute the matrix A = (a_ij), where

        a_ij = 1/2 * || x_i - y_j ||^2

    uses library function scipy.spatial.distance.cdist

    Parameters
    ----------
    x, y:
      Arrays of shape (n, d) and (m, d), respectively,
      interpreted as collections of n (resp. m) points
      in R^d.

      y is optional. distsquared(x) is equivalent to
      distsquared(x, x).

    Returns
    -------
    Array of shape (n, m)
    """
    from scipy.spatial.distance import cdist
    distmatrix = cdist(x[:, None], y[:, None])**2
    return distmatrix

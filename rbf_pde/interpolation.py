#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: jschulz1
"""

import numpy as np
from numpy.linalg import solve
from rbf_pde.distances import distsquared
from rbf_pde.rbf import Gaussian
from numpy.random import default_rng


def interpolation(nodes, fnode, rbf):
    """ Computes an 2D interpolation from known points.

    With known points `nodes` and given function values `fnodes` `interpolation`
    computes the interpolated values at `interpoints` using radial basis functions.

    Parameters
    ----------
    nodes : array_like
        (m x 2) array of points for known function values.
    interpoints : array_like
        (n x 2) array of points where to interpolate.
    fnodes : array_like
        (m x 1) array of function values on `nodes`.
    rbf : function or class
        function or class to evaluate node values

    Returns
    -------
    res : array_like
        interpolated function values on `interpoints`.

    Examples
    --------

    >>> f = lambda x: np.sin(x[:, 0])*np.cos(x[:, 1])  # true function
    >>> rbfpoints = np.random.rand(20, 2)  # points with known function
    >>> X, Y = np.mgrid[0:1:30j, 0:1:30j]
    >>> gridpoints = np.vstack([X.ravel(), Y.ravel()]).T  # array (mx2) of points
    >>> res = interpolation.interpolation(rbfpoints, gridpoints, f(rbfpoints))
    """


class Interp_rbf:
    def __init__(self, nodes, fnodes, rbf):
        self.rbf = rbf
        self.nodes = nodes

        distmatrix = distsquared(nodes)  # distance matrix
        A = rbf(distmatrix)
        self.coeff = solve(A, fnodes)  # solve linear system for coefficents
        return

    def __call__(self, x, y):
        # now, having the coefficents interpolate on the given points y
        # distance matrix for nodes to interppoints
        dsqy = distsquared(np.stack([x.ravel(), y.ravel()], axis=1), self.nodes)  
        B = rbf(dsqy)
        res = B @ self.coeff
        return res.reshape(x.shape[0], x.shape[0])


def createnodes(nrnodes):
    """ function Creates nodes
    Parameters:
        nrnodes:
    Return:
        nodes, shape (m,2)
    random numbers for the node generation inside choosen region \omega
    """
    rng = default_rng()
    nodes = rng.random((nrnodes, 2))
    return nodes


def f(x):
    return np.sin(x[:, 0])*np.cos(x[:, 1])  # true function


if __name__ == "__main__":
    nodes = createnodes(40)
    fnodes = np.sin(nodes[:, 0])*np.cos(nodes[:, 1])
    # y = f(nodes)  # true solution
    rbf = Gaussian()
    interpf = Interp_rbf(nodes, fnodes, rbf)

    nrgridpoints = 60
    xx, yy = np.mgrid[0:1:nrgridpoints*1j, 0:1:nrgridpoints*1j]
    interpolated_vals = interpf(xx, yy)

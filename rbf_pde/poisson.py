#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 16 16:03:15 2018

@author: jschulz1
"""

import numpy as np
from numpy.linalg import lstsq
from scipy.spatial.distance import cdist
from rbf_pde.rbf import Gaussian


def laplace(distm, rbf, dim=2):
    # return rbf(distm, derivative=2) + (dim - 1)/distm*rbf(distm, derivative=1)
    return 2*distm*rbf(distm, derivative=2) + 2*rbf(distm, derivative=1)


def poisson(nodes=None, points=None):
    # nrn = 5
    # get nodes
    # nod = Nodes(nrn)
    distm_inner = cdist(nodes.inner, nodes.p())**2
    distm_bound = cdist(nodes.boundary, nodes.p())**2
    # print(distm_inner)
    # print(distm_bound)
    rbf = Gaussian()
    # setup matrix
    Ai = laplace(distm_inner, rbf)
    Ab = rbf(distm_bound)
    A = np.vstack((Ai, Ab))
    b = np.hstack((np.ones((nodes.nr, )), np.zeros((nodes.nr, ))))
    # solve system
    print(A.shape)
    print(b.shape)
    res, residual, rank, s = lstsq(A, b, rcond=None)
    print(res)
    # img = hv.Image(np.sin(xx)*np.cos(yy))
    distmatrix = cdist(points, nodes.p())**2
    return rbf(distmatrix) @ res

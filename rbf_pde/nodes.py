import numpy as np


class Nodes:
    """ class to hold nodes

    expect points to be x-dimensional array
    holds distmatrix
    """
    def __init__(self, nr, dim=2, shape='circle'):
        self.inner = np.random.randn(nr, 2)
        p = np.linspace(0, 2*np.pi, nr)
        self.boundary = np.vstack((np.sin(p), np.cos(p))).T
        self.nr = nr
        return

    def p(self):
        return np.vstack((self.inner, self.boundary))

    def __repr__(self):
        return str(self.inner) + str(self.boundary)
